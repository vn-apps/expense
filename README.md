# README 

# Run migrations database
----------------------------------------
$ php artisan migrate or php artisan migrate --path=app/foo/migrations

# Not found command "LL"
------------------------------------------
Everything what you have to do is execute this command and restart terminal

$ echo "alias ll='ls -l'" >> /etc/bash.bashrc

# Module in laravel
------------------------------------------
https://stackoverflow.com/questions/28485690/how-to-structure-a-modular-app-in-laravel-5

# Eloquents in Laravel
-------------------------------------------
https://viblo.asia/p/repository-pattern-trong-laravel-07LKX2RDlV4

# Not display csf_token add middleware into Route
-------------------------------------------
Route::group(['prefix' => 'users', 'middleware' => 'web'], function () {
}

# https://fractal.thephpleague.com/
-------------------------------------------
Link https://fractal.thephpleague.com/
-------------------------------------------