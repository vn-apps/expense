<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string password
 * @property string avatar
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    
    const TYPE = [
        'NORMAL' => 0,
        'DRIVER' => 1
    ];

    const TYPE_STRING = [
        'NORMAL' => 'User',
        'DRIVER' => 'Driver'
    ];

    const STATUS = [
        'UNVERIFIED' => 1,
        'DISABLED' => 2,
        'LOCKED' => 4
    ];

    const STATUS_DISPLAY = [
        'ACTIVE' => 'Active',
        'UNVERIFIED' => 'Unverified',
        'VERIFIED' => 'Verified',
        'DISABLED' => 'Disabled',
        'ENABLED' => 'Enabled',
        'LOCKED' => 'Locked',
        'UNLOCKED' => 'Unlocked'
    ];

    const STATUS_MAPPING = [
        'ACTIVE' => 0, //verfied + enabled + unlocked
        'DISABLED' => [
            2, //verified + disabled + unlocked
            3, //unverified + disabled + unlocked
            6, //verified +  disabled + locked
            7, //unverified + disabled + locked
        ],
        'LOCKED' => [4], //verified + enabled + locked
        'UNVERIFIED' => [
            1, //unverified + enabled + unlocked
            5 //unverified + enabled + locked
        ],
    ];

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $table = 'users';

    public function categories()
    {
        return $this->hasMany(Category::class);
    }
}
