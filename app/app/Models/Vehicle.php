<?php
declare(strict_types=1);

namespace App\Models;

class Vehicle extends Model
{
    protected $guarded = ['id'];

    public $fillable = ['brand', 'model_name', 'license_plate', 'color', 'registration_year'];

   // public $timestamps = true;

    public $table = 'vehicles';
}
