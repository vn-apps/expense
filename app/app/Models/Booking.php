<?php
declare(strict_types=1);

namespace App\Models;

class Booking extends Model
{
    protected $guarded = ['id'];

    public $table = 'booking';
}
