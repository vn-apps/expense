<?php
declare(strict_types=1);

namespace App\Models;

/**
 * @property int id
 * @property string name
 * @property int parent_id
 * @property mixed status
 */
class Company extends Model
{
    protected $guarded = ['id'];

    const STATUS_ACTIVE = 1;

    public $table = 'companies';
}
