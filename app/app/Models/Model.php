<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class Model extends BaseModel
{
    /** @var bool */
    public $timestamps = false;

    /** @var string */
    const CREATED_AT = 'created';

    /** @var string */
    const UPDATED_AT = 'modified';

    /** @var array */
    protected $dates = [
        'created',
        'modified'
    ];

    public static function boot()
    {
        parent::boot();
        
        static::creating(function (BaseModel $model) {
            $table = $model->getTable();
            if (Schema::hasColumn($table, 'created')) {
                $model->created = new Carbon;
            }
            if (Schema::hasColumn($table, 'modified')) {
                $model->modified = new Carbon;
            }
        });

        static::saving(function (BaseModel $model) {
            $table = $model->getTable();
            if (Schema::hasColumn($table, 'modified')) {
                $model->modified = new Carbon;
            }
        });
    }
}
