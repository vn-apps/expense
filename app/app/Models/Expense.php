<?php
declare(strict_types=1);

namespace App\Models;

/**
 * @property int id
 * @property string name
 * @property string description
 * @property double $amount
 * @property int $category_id
 * @property int $user_id
 */
class Expense extends Model
{
    protected $guarded = ['id'];
    //protected $fillable = array('name', 'description');

    public $table = 'expenses';
}
