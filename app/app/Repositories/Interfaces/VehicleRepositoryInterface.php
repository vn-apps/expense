<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface VehicleRepositoryInterface extends RepositoryInterface
{
    public function getListVehicles(array $request);

    public function create(array $data);
}
