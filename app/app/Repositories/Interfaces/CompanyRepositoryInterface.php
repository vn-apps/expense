<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface CompanyRepositoryInterface extends RepositoryInterface
{
    public function getListCompanies(array $request);

    public function create(array $data);
}
