<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface BookingRepositoryInterface extends RepositoryInterface
{
    public function getListBooking();

    public function create(array $data);
}
