<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * @return object
     */
    public function getListCategories(array $request);

    public function create(array $data);
}
