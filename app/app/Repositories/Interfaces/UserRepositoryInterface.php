<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface extends RepositoryInterface
{
    public function getListUsers($perPage);

    //public function createUser(array $user);

    // public function getAll();

    // public function getById($id);

    public function create(array $attribute);

    // public function update($id, array $attribute);

    // public function delete($id);
}

