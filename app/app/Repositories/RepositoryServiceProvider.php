<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\Booking;
use App\Models\Category;
use App\Models\Company;
use App\Models\User;
use App\Models\Vehicle;
use App\Repositories\Eloquents\BookingRepositoryEloquent;
use App\Repositories\Eloquents\CategoryRepositoryEloquent;
use App\Repositories\Eloquents\CompanyRepositoryEloquent;
use App\Repositories\Eloquents\UserRepositoryEloquent;
use App\Repositories\Interfaces\BookingRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\CompanyRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\VehicleRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /** @var bool */
    protected $defer = true;

    public function register()
    {
        $this->app->bind(CompanyRepositoryInterface::class, function () {
            return new CompanyRepositoryEloquent(new Company());
        });

        $this->app->bind(CategoryRepositoryInterface::class, function () {
            return new CategoryRepositoryEloquent(new Category());
        });

        $this->app->bind(UserRepositoryInterface::class, function (){
            return new UserRepositoryEloquent(new User());
        });

        $this->app->bind(VehicleRepositoryInterface::class, function (){
            return new VehiCleRepositoryEloquent(new Vehicle());
        });

        $this->app->bind(BookingRepositoryInterface::class, function (){
            return new BookingRepositoryEloquent(new Booking());
        });
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            CompanyRepositoryInterface::class,
            CategoryRepositoryInterface::class,
            UserRepositoryInterface::class,
        ];
    }
}
