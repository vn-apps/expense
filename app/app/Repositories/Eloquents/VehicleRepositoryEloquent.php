<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\VehicleRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VehicleRepositoryEloquent extends RepositoryEloquent implements VehicleRepositoryInterface
{
    /**
     * 
     * @param array $request
     * @return type
     * @throws \InvalidArgumentException
     */
    public function getListVehicles(array $request)
    {
        try {
            $perPage = $request['per_page'] ?? 25;
            $vehicles = $this->model
                            ->select('*');

            return $vehicles->paginate($perPage);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     * @throws \InvalidArgumentException
     */
    public function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }
}
