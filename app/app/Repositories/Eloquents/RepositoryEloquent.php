<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

abstract class RepositoryEloquent implements RepositoryInterface
{
    // model property on class instances
    public $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

   /**
    * Get all instances of model
    * @return object
    */
   public function all()
   {
       echo "all"; exit;
       return $this->model->all();
   }

   /**
    * Create a new record in the database
    * @param array $data
    * @return object
    */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

   /**
    * Update record in the database
    *
    * @param array $data
    * @param int $id
    * @return object
    */
   public function update(array $data, $id)
   {
       $record = $this->find($id);
    
       return $record->update($data);
   }

   /**
    * Remove record from the database
    * @param int $id
    * @return object
    */
   public function delete($id)
   {
       return $this->model->destroy($id);
   }

   /**
    * Show the record with the given id
    *
    * @param int id
    * @return object
    */
   public function find($id)
   {
       return $this->model->findOrFail($id);
   }

   /**
    * Get the associated model
    */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the associated model
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Eager load database relationships
     */
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
