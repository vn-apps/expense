<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryRepositoryEloquent extends RepositoryEloquent implements CategoryRepositoryInterface
{
    public function getListCategories(array $request)
    {
        try {
            $perPage = $request['per_page'] ?? 20;
            $categories = $this->model
                ->select('id', 'name', 'status');

            return $categories->paginate($perPage);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }

    public function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }
}
