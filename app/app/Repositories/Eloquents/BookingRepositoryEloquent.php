<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\BookingRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookingRepositoryEloquent extends RepositoryEloquent implements BookingRepositoryInterface
{

    public function getListBooking()
    {
        try {
            $companies = $this->model
                            ->select('*');

            return $companies->get();
        } catch (ModelNotFoundException $ex) {
            echo "ModelNotFoundException"; exit;
            throw new \InvalidArgumentException("Could not find user.");
        }
    }

    public function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }
}

