<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Interfaces\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompanyRepositoryEloquent extends RepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * 
     * @param array $request
     * @return type
     * @throws \InvalidArgumentException
     */
    public function getListCompanies(array $request)
    {
        try {
            $perPage = $request['per_page'] ?? 25;

            $companies = $this->model
                            ->select('id', 'name', 'status');

            return $companies->paginate($perPage);
        } catch (ModelNotFoundException $ex) {
            echo "ModelNotFoundException"; exit;
            throw new \InvalidArgumentException("Could not find user.");
        }
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (ModelNotFoundException $ex) {
            throw new \InvalidArgumentException("Could not find user.");
        }
    }
}
