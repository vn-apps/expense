<?php
declare(strict_types=1);

namespace App\Repositories\Eloquents;

use App\Repositories\Eloquents\RepositoryEloquent;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use InvalidArgumentException;

class UserRepositoryEloquent extends RepositoryEloquent implements UserRepositoryInterface
{
    public function create(array $attribute)
    {
        try {
            return $this->model->create($attribute);
        } catch (ModelNotFoundException $ex) {
            echo "exception";
            exit;
            throw new InvalidArgumentException("Could not find user");
        }
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     * @throw InvalidArgumentException
     */
    public function getListUsers($perPage)
    {
        try {
            return $this->model->paginate($perPage);

            //return $this->model->get();
        } catch (ModelNotFoundException $ex) {
            throw new InvalidArgumentException($ex->getMessage());
        }
    }
}
