<?php

namespace App\Transformers;

use App\Models\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        return [
            'id' => $company['id'],
            'name' => $company['name'],
            'parent_id'=> $company['parent_id'] ?? 0,
            'address' => $company['address'],
            'phone' => $company['phone'],
            'fax' => $company['fax'],
            'logo' => $company['logo'],
            'introduction' => $company['introduction'],
            'status'=> $company['status']
        ];
    }
}
