<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class SignInTransformer extends TransformerAbstract
{
    public function transform(array $data) : array
    {
        return [
            'access_token' => '',
            'refresh_token' => '',
            'token_type' => 'bearer',
            'expires_in' => ''
        ];
    }
}