<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform($user)
    {
        return [
            'id' => $user['id'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'email' => $user['email'],
            'avatar' => $user['avatar'],
            'role' => $user['role'],
            'company_id' => $user['company_id'],
            'type' => $this->getUserTypeByNumber($user['type']),
            'group_id' => $user['group_id'],
            'status' => $this->getStatusByNumber($user['status'] ?? 0)
        ];
    }

    /**
     * @param int $number
     * @return string
     */
    private function getStatusByNumber($number) : string
    {
        $valueArr = array_values(User::STATUS_MAPPING);
        $valueArr[] = User::STATUS_MAPPING['ACTIVE'];

        if (!in_array($number, $valueArr, true)) {
            return '';
        }

        if (in_array($number, User::STATUS_MAPPING['DISABLED'], true)) {
            return User::STATUS_DISPLAY['DISABLED'];
        }

        if (in_array($number, User::STATUS_MAPPING['UNVERIFIED'], true)) {
            return User::STATUS_DISPLAY['UNVERIFIED'];
        }

        if (in_array($number, User::STATUS_MAPPING['LOCKED'], true)) {
            return User::STATUS_DISPLAY['LOCKED'];
        }

        return User::STATUS_DISPLAY['ACTIVE'];
    }

    /**
     * @param $number
     * @return string
     */
    private function getUserTypeByNumber($number): string
    {
        switch ($number) {
            case User::TYPE['NORMAL']:
                $result = User::TYPE_STRING['NORMAL'];
                break;
            case User::TYPE['DRIVER']:
                $result = User::TYPE_STRING['DRIVER'];
                break;
            default:
                $result = '';
                break;
        }

        return $result;
    }
}
