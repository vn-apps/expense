<?php

namespace App\Transformers;

use App\Models\Booking;
use League\Fractal\TransformerAbstract;

class BookingTransformer extends TransformerAbstract
{
    public function transform(Booking $booking)
    {
        return [
            'id' => $booking['id'],
            'name' => $booking['name'],
            'destination' => $booking['destination'],
            'driver_id' => $booking['driver_id'],
            'vehicle_type' => $booking['vehicle_type'],
            'note' => $booking['note'],
            'status' => $booking['status']
        ];
    }

}
