<?php

namespace App\Transformers;

use App\Models\Vehicle;
use League\Fractal\TransformerAbstract;

class VehicleTransformer extends TransformerAbstract
{
    public function transform(Vehicle $vehicle)
    {
        return [
            'id' => $vehicle['id'],
            'brand' => $vehicle['brand'],
            'model_name' => $vehicle['model_name'],
            'license_plate' => $vehicle['license_plate'],
            'color' => $vehicle['color'],
            'registration_year' => $vehicle['registration_year'],
            'description' => $vehicle['description'],
            'picture' => $vehicle['picture'],
            'type' => $vehicle['type'],
            'status' => $vehicle['status']
        ];
    }

}
