<?php
declare(strict_types=1);

namespace App\Library\Hmac;

class RequestUtils
{
    /**
     * Canonically format request elements
     * @see http://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
     */

    /**
     * Canonicalized Headers
     */
    protected static $_canonicalizedHeaders;

    /**
     * Canonicalized Header List
     */
    protected static $_canonicalizedHeaderList;

    public static function init()
    {
        self::$_canonicalizedHeaders = '';
        self::$_canonicalizedHeaderList = '';
    }

    /**
     * Parse resource into the proper format
     * e.g.:
     * urlencode(<resource>)
     *
     * @param string $resourceUrl Resurce url
     * @return string Canonicalized resource url
     */
    public static function getCanonicalizedResourceUrl($resourceUrl)
    {
        if (!$resourceUrl) {
            return '/';
        }

        $doubleEncoded = rawurlencode(ltrim($resourceUrl, '/'));

        return '/' . str_replace('%2F', '/', $doubleEncoded);
    }

    /**
     * Parse query string into the proper format
     * e.g.:
     * urlencode(<param_name>) . '=' . urlencode(<param_value>) . '&' ...
     *
     * @param string $queryString Query string
     * @return string Canonicalized query string
     */
    public static function getCanonicalizedQueryString($queryString)
    {
        if (!$queryString) {
            return '';
        }

        $canonicalizedQueryString = '';
        $queryParams = array();
        parse_str($queryString, $queryParams);
        ksort($queryParams);

        foreach ($queryParams as $key => $values) {
            if (is_array($values)) {
                sort($values);
            } elseif ($values === 0) {
                $values = array('0');
            } elseif (!$values) {
                $values = array('');
            }

            foreach ((array) $values as $value) {
                $canonicalizedQueryString .= rawurlencode($key) . '=' . rawurlencode($value) . '&';
            }
        }

        return substr($canonicalizedQueryString, 0, -1);
    }

    /**
     * Parse indexed header array into the proper format
     * e.g.:
     * strtolower(<header_name>) . ':' . trim(<header_value>) . "\n" ...
     *
     * @param array $headers Numerically indexed list of headers, in php curl style format,
     * @param string $headerNamespace The namespace that is being used for custom headers
     * e.g.: array('<header_name>: <header_value>', '<header_name>: <header_value>')
     * @return string List of canonicalized headers, sorted alphabetically
     */
    public static function getCanonicalizedHeaders(array $headers, $headerNamespace)
    {
        if (self::$_canonicalizedHeaders) {
            return self::$_canonicalizedHeaders;
        }

        $signable = array(
            'host'         => true,
            'date'         => true,
            'content-type' => true,
            'content-md5'  => true
        );

        $canonicalizedHeaders = array();

        $providedHeaders = self::parseHeaderIndexedArrayToAssocArray($headers);

        foreach ($providedHeaders as $headerName => $headerValue) {
            $headerName = strtolower($headerName);
            $customHeaderPrefix = 'x-' . $headerNamespace . '-';

            if (isset($signable[$headerName]) || stripos($headerName, $customHeaderPrefix) !== false) {
                $canonicalizedHeaders[$headerName] = $headerName . ':' . preg_replace('/\s+/', ' ', $headerValue);
            }
        }

        if (!$canonicalizedHeaders) {
            $msg = "Could not parse the provided header array into canonicalized headers: " .
                addslashes(json_encode($headers));
            throw new TrinetCloudException($msg);
        }

        ksort($canonicalizedHeaders);

        self::$_canonicalizedHeaderList = implode(';', array_keys($canonicalizedHeaders));
        self::$_canonicalizedHeaders = implode("\n", $canonicalizedHeaders) . "\n";

        return self::$_canonicalizedHeaders;
    }

    /**
     * Parse indexed header array into the proper format
     * e.g.:
     * strtolower(<header_name>) . ';' . strtolower(<header_name>) ...
     *
     * @param array $headers Numerically indexed list of headers, in php curl style format
     * @return string List of canonicalized header names, sorted alphabetically
     */
    public static function getCanonicalizedHeaderList(array $headers, $headerNamespace)
    {
        if (self::$_canonicalizedHeaderList) {
            return self::$_canonicalizedHeaderList;
        }

        self::getCanonicalizedHeaders($headers, $headerNamespace);

        return self::$_canonicalizedHeaderList;
    }

    /**
     * Parse php curl style headers into assoc array
     *
     * Make keys lower case
     *
     * e.g.: array('<header_name>: <header_value>', '<header_name>: <header_value>') TO
     * array(<header_name> => <header_value>, <header_name> => <header_value>)
     *
     * @see http://php.net/manual/en/function.curl-setopt.php
     * @param array $headers Numerically indexed list of headers, in php curl style format
     * @return string List of canonicalized header names
     */
    public static function parseHeaderIndexedArrayToAssocArray(array $headerArray)
    {
        $assocHeaderArray = [];

        foreach ($headerArray as $header) {
            if (strpos($header, ':') === false) {
                $msg = "Could not parse the provided header array, no ':' delimeter is present";
                throw new TrinetCloudException($msg);
            }

            $headerName = substr($header, 0, strpos($header, ':'));
            $headerValue = substr($header, strpos($header, ':') + 1);

            if ($headerName === false || $headerValue === false) {
                $msg = "Could not parse the provided header array: ".addslashes(json_encode($headerArray));
                throw new TrinetCloudException($msg);
            }

            $assocHeaderArray[ strtolower(trim($headerName)) ] = trim($headerValue);
        }

        return $assocHeaderArray;
    }

    /**
     * Check an array for missing keys
     *
     * @param array $inputArray Input array to check
     * @param array $requiredKeys Required keys
     * @return mixed Return missing keys as array, else return false
     */
    public static function getMissingArrayKeys(array $inputArray, array $requiredKeys)
    {
        $missingKeys = [];

        $inputArrayKeys = array_keys($inputArray);
        
        $requiredKeys = array_map('strtolower', $requiredKeys);
        $inputArrayKeys = array_map('strtolower', $inputArrayKeys);

        $missingKeys = array_diff($requiredKeys, $inputArrayKeys);

        return $missingKeys;
    }
}
