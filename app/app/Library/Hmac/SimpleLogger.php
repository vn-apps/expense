<?php
declare(strict_types=1);

namespace App\Library\Hmac;

use Psr\Log\LoggerInterface;

class SimpleLogger
{
    const EMERGENCY = 'emergency';
    const ALERT = 'alert';
    const CRITICAL = 'critical';
    const ERROR = 'error';
    const WARNING = 'warning';
    const NOTICE = 'notice';
    const INFO = 'info';
    const DEBUG = 'debug';

    protected static $_defaultLevels = [
        'emergency' => array(LOG_EMERG),
        'alert' => array(LOG_ALERT, LOG_EMERG),
        'critical' => array(LOG_CRIT, LOG_ALERT, LOG_EMERG),
        'error' => array(LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG),
        'warning' => array(LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG),
        'notice' => array(LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG),
        'info' => array(LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG),
        'debug' => array(LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG)
    ];

    const OPERATING_SYSTEM = 0;
    const SAPI = 4;

    protected static $_messageType = self::OPERATING_SYSTEM;

    protected static $_currentLevel = self::ERROR;

    /** @var LoggerInterface */
    private static $logger;

    public static function setLogLevel($level)
    {
        self::$_currentLevel = $level;
    }

    public static function setLogger(LoggerInterface $logger = null)
    {
        self::$logger = $logger;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     */
    public static function emergency($message)
    {
        if (self::allowWrite(LOG_EMERG)) {
            self::write($message, self::EMERGENCY);
        }
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     */
    public static function alert($message)
    {
        if (self::allowWrite(LOG_ALERT)) {
            self::write($message, self::ALERT);
        }
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     */
    public static function critical($message)
    {
        if (self::allowWrite(LOG_CRIT)) {
            self::write($message, self::CRITICAL);
        }
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     */
    public static function error($message)
    {
        if (self::allowWrite(LOG_ERR)) {
            self::write($message, self::ERROR);
        }
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     */
    public static function warning($message)
    {
        if (self::allowWrite(LOG_WARNING)) {
            self::write($message, self::WARNING);
        }
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     */
    public static function notice($message)
    {
        if (self::allowWrite(LOG_NOTICE)) {
            self::write($message, self::NOTICE);
        }
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     */
    public static function info($message)
    {
        if (self::allowWrite(LOG_INFO)) {
            self::write($message, self::INFO);
        }
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     */
    public static function debug($message)
    {
        if (self::allowWrite(LOG_DEBUG)) {
            self::write($message, self::DEBUG);
        }
    }

    protected static function allowWrite($level)
    {
        if (isset(self::$_defaultLevels[self::$_currentLevel]) &&
            in_array($level, self::$_defaultLevels[self::$_currentLevel])
        ) {
            return true;
        }

        return false;
    }


    protected static function formatMessage($message, $level, $name = "TrinetCloud")
    {
        return sprintf("%s.%s: %s", $name, $level, $message);
    }

    protected static function write($message, $level)
    {
        $formattedMessage = self::formatMessage($message, $level);
        if (self::$logger) {
            $loweredLevel = strtolower($level);
            if ($loweredLevel === 'emergency') {
                self::$logger->emergency($formattedMessage);
            } elseif ($loweredLevel === 'alert') {
                self::$logger->alert($formattedMessage);
            } elseif ($loweredLevel === 'critical') {
                self::$logger->critical($formattedMessage);
            } elseif ($loweredLevel === 'error') {
                self::$logger->error($formattedMessage);
            } elseif ($loweredLevel === 'warning') {
                self::$logger->warning($formattedMessage);
            } elseif ($loweredLevel === 'notice') {
                self::$logger->notice($formattedMessage);
            } elseif ($loweredLevel === 'info') {
                self::$logger->info($formattedMessage);
            } elseif ($loweredLevel === 'debug') {
                self::$logger->debug($formattedMessage);
            }
        } else {
            error_log($formattedMessage, self::$_messageType);
        }
    }
}
