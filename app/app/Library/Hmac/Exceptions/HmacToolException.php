<?php
namespace App\Library\Hmac\Exceptions;

use App\Library\Hmac\Exceptions\CloudException;

class HmacToolException extends CloudException
{
}
