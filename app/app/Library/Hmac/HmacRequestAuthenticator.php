<?php
declare(strict_types=1);

namespace App\Library\Hmac;

use App\Library\Hmac\Exceptions\AuthException;
use App\Library\Hmac\SimpleLogger;
use App\Library\Hmac\HmacTool;
use App\Library\Hmac\RequestUtils;

/**
 * Authenticate a HMAC (Hashed Message Authentication Code) HTTP Request
 *
 * Note:  * Generate keys in CakePHP using command: php cake.php authkey_generator
 *
 * --- Example 1: Basic authentication ---
 *
 * HmacRequestAuthenticator::init();
 *
 * $publicKey = HmacRequestAuthenticator::getPublicKey();
 * $secretKey = mapPublicKeyToSecretKey($publicKey);
 *
 * $authRequestObj = new HmacRequestAuthenticator($publicKey, $secretKey);
 *
 * $authRequestObj->authenticate();
 * [allow authenticated request]
 *
 * --- Example 2: Using payload ---
 *
 * HmacRequestAuthenticator::init();
 *
 * $publicKey = HmacRequestAuthenticator::getPublicKey();
 * $secretKey = mapPublicKeyToSecretKey($publicKey);
 * $payload = 'param1=hello&param2=world';
 *
 * $authRequestObj = new HmacRequestAuthenticator($publicKey, $secretKey, $payload);
 *
 * $authRequestObj->authenticate();
 * [allow authenticated request]
 */
class HmacRequestAuthenticator
{
    protected static $_debug;

    protected static $_headerNamespace;
    protected static $_phpAuthorizationHeaderName;

    protected static $_requiredPhpServerHeaders;
    protected static $_requiredSignedHeaders;

    protected static $_parsedRequestHeaders;

    protected static $_requestExpiresAfter;
    protected static $_authorizatonHeaderExpiresAfter;

    protected $_authenticationTool;

    protected $_publicKey;
    protected $_secretKey;

    /**
     * Instantiate object
     *
     * @param string $publicKey Apps public key
     * @param string $secretKey Apps secret key
     * @param string $requestPayload Payload in plain text
     * @throws \TriNetCloud\Common\Exception\Hmac\AuthException
     */
    public function __construct($publicKey = '', $secretKey = '', $requestPayload = '')
    {
        if (!$publicKey || !$secretKey) {
            throw new AuthException('Missing API Key(s)');
        }

        $this->_publicKey = $publicKey;
        $this->_secretKey = $secretKey;

        //Configure HmacTool with public/private keys, payload, and request data
        $settings['public_key'] = $this->_publicKey;
        $settings['secret_key'] = $this->_secretKey;
        $settings['payload'] = $requestPayload;

        SimpleLogger::debug("HmacRequestAuthenticator::authenticate --- Server HMAC Auth Validate: public key '{$this->_publicKey}' mapped to secret key '{$this->_secretKey}'");
        SimpleLogger::debug("HmacRequestAuthenticator::authenticate --- Payload:\n{$requestPayload}");

        $settings['resource_url'] = self::$_parsedRequestHeaders['resource_url'];
        $settings['http_method'] = self::$_parsedRequestHeaders['http_method'];
        $settings['hmac_http_headers'] = self::$_parsedRequestHeaders['headers'];
        $settings['algorithm'] = self::$_parsedRequestHeaders['parsed-x-authorization']['algorithm'];

        $this->_authenticationTool = new HmacTool($settings);
    }

    /**
     * Update object state with current header data
     */
    public static function init()
    {
        $debug = true;

        SimpleLogger::debug('HmacRequestAuthenticator::init --- Server HMAC Auth Validate: Initializing HMAC Request authorization...');

        self::$_headerNamespace = 'tcd';

        $phpDateHeader = 'HTTP_X_' . strtoupper(self::$_headerNamespace) . '_DATE';
        $dateHeader = 'x-'. self::$_headerNamespace .'-date';

        self::$_requiredPhpServerHeaders = ['REQUEST_METHOD' ,'HTTP_HOST', 'REQUEST_URI', $phpDateHeader, "HTTP_X_AUTHORIZATION"];
        self::$_requiredSignedHeaders = ['host', $dateHeader];

        //self::_validateHeaders(); //Comments out 01

        self::$_phpAuthorizationHeaderName = 'HTTP_X_AUTHORIZATION';

        //self::_parseRequestHeaders(); //Comments out 01

        self::$_requestExpiresAfter = 5; //minutes
        self::$_authorizatonHeaderExpiresAfter = 10080; //7 days in minutes

        SimpleLogger::debug('HmacRequestAuthenticator::init --- Server HMAC Auth Validate: HMAC Request authorization initialized.');
    }

    /**
     * Validate that the $_SERVER header super global contains all of the required headers as defined by
     * HmacRequestAuthenticator::$_requiredPhpServerHeaders.
     */
    protected static function _validateHeaders()
    {
        SimpleLogger::debug("HmacRequestAuthenticator::_validateHeaders --- Received headers: " . json_encode($_SERVER));

        $missingHeaders = RequestUtils::getMissingArrayKeys($_SERVER, self::$_requiredPhpServerHeaders);

        if ($missingHeaders) {
            SimpleLogger::debug("HmacRequestAuthenticator::_validateHeaders --- Required headers are missing: ".json_encode($missingHeaders));

            throw new AuthException("Required headers are missing: " . addslashes(json_encode(array_values($missingHeaders))));
        }
    }

    /**
     * Parse $_SERVER headers
     *
     * Headers will be stored as a numerically indexed list and a map where the header name is the key and the header value is the value.
     */
    protected static function _parseRequestHeaders()
    {

        SimpleLogger::debug('HmacRequestAuthenticator::_parseRequestHeaders --- Server HMAC Auth Validate: Parsing request header for authorization credentials...');

        self::$_parsedRequestHeaders['resource_url'] = self::_getUrlFromRequest();
        self::$_parsedRequestHeaders['http_method'] = $_SERVER['REQUEST_METHOD'];

        self::_parseAuthorizationHeader();

        self::$_parsedRequestHeaders['headers'] = self::_normalizeRequestHeaders();
        self::$_parsedRequestHeaders['header_map'] = RequestUtils::parseHeaderIndexedArrayToAssocArray(self::$_parsedRequestHeaders['headers']);

        SimpleLogger::debug("HmacRequestAuthenticator::_parseRequestHeaders --- Server HMAC Auth Validate: Parsed request header: " . json_encode(self::$_parsedRequestHeaders));
    }

    /**
     * Get the full url from the current request
     */
    protected static function _getUrlFromRequest()
    {

        $protocol = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) ? 'https' : 'http';

        return "{$protocol}://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    }

    /**
     * Normalize $_SERVER headers into a numerically indexed array of headers in php curl style format
     * e.g.: array('<header_name>: <header_value>', '<header_name>: <header_value>')
     *
     * Parse out the HTTP headers in $_SERVER: keys prepended with 'HTTP_'
     *
     * @see http://php.net/manual/en/function.curl-setopt.php
     */
    protected static function _normalizeRequestHeaders()
    {
        $normalizedRequestHeaders = array();

        foreach ($_SERVER as $infoType => $value) {
            $headerName = strtolower(str_replace('_', '-', str_replace('HTTP_', '', $infoType)));

            if (in_array($headerName, self::$_parsedRequestHeaders['parsed-x-authorization']['signed_headers'])) {
                $normalizedRequestHeaders[$headerName] = "{$headerName}: {$value}";
            }
        }

        $missingClientSignedHeaders = RequestUtils::getMissingArrayKeys(
            $normalizedRequestHeaders,
            self::$_parsedRequestHeaders['parsed-x-authorization']['signed_headers']
        );

        if ($missingClientSignedHeaders) {
            SimpleLogger::debug("HmacRequestAuthenticator::_validateHeaders --- Headers that were part of the client signature are missing: ".json_encode($missingClientSignedHeaders));

            throw new AuthException("Headers that were part of the client signature are missing: " . addslashes(json_encode(array_values($missingClientSignedHeaders))));
        }

        return array_values($normalizedRequestHeaders);
    }

    /**
     * Parse the X-Authorization request header
     *
     * e.g.: "X-Authorization: TCD-HMAC-SHA256 Credential=<public_key>/<YYYYMMDD>/<service>/tcd_request, SignedHeaders=<signed_headers>, Signature=signature"
     *
     * Parse the authorizaton header for:
     * - algorithm
     * - public_key
     * - date
     * - signed_headers (as array)
     * - signature
     */
    protected static function _parseAuthorizationHeader()
    {

        $validAuthHeaderParts = self::_validateAndParseAuthorizationHeader();

        self::$_parsedRequestHeaders['parsed-x-authorization']['algorithm'] = $validAuthHeaderParts['algorithm'];
        self::$_parsedRequestHeaders['parsed-x-authorization']['public_key'] = $validAuthHeaderParts['credential_parts'][0];
        self::$_parsedRequestHeaders['parsed-x-authorization']['date'] = $validAuthHeaderParts['credential_parts'][1];
        self::$_parsedRequestHeaders['parsed-x-authorization']['signed_headers'] = $validAuthHeaderParts['signed_headers_list'];
        self::$_parsedRequestHeaders['parsed-x-authorization']['signature'] = $validAuthHeaderParts['signature'];
    }

    /**
     * Validate and parse the x-authorization header
     *
     * e.g.: "X-Authorization: TCD-HMAC-SHA256 Credential=<public_key>/<YYYYMMDD>/<service>/tcd_request, SignedHeaders=<signed_headers>, Signature=signature"
     *
     * @return array Validated parts of the X-Authorization header
     * @throws \TriNetCloud\Common\Exception\Hmac\AuthException
     */
    protected static function _validateAndParseAuthorizationHeader()
    {
        $authorizationHeader = trim($_SERVER[self::$_phpAuthorizationHeaderName]);

        if (stripos($authorizationHeader, 'Credential=') === false ||
            stripos($authorizationHeader, 'SignedHeaders=') === false ||
            stripos($authorizationHeader, 'Signature=') === false
        ) {
            throw new AuthException("The authorization header is incomplete");
        }

        $authHeaderPieces = preg_split('/\s|,\s|,/i', $authorizationHeader);

        if (count($authHeaderPieces) != 4) {
            throw new AuthException("The authorization header could not be parsed");
        }

        $algorithm = $authHeaderPieces[0];

        if (stripos($algorithm, '-HMAC-SHA256') === false) {
            throw new AuthException("The authorization header algorithm is not valid");
        }

        $credentialParts = array_map('trim', explode('/', str_ireplace('Credential=', '', $authHeaderPieces[1])));

        if (count($credentialParts) != 4) {
            throw new AuthException("The authorization header Credential parameter could not be parsed");
        }

        if (!$credentialParts[0]) {
            throw new AuthException("A public key was not provided in the authorization header Credential parameter");
        }

        if (preg_match('/[0-9]{8}/', $credentialParts[1]) !== 1) {
            throw new AuthException("The authorization header Credential parameter does not contain a valid date: YYYYMMDD");
        }

        $signedHeaderParts = array_map('trim', explode(';', str_ireplace('SignedHeaders=', '', $authHeaderPieces[2])));

        foreach (self::$_requiredSignedHeaders as $requiredHeader) {
            if (!in_array($requiredHeader, $signedHeaderParts)) {
                throw new AuthException("The authorization header SignedHeaders parameter is missing a required header: {$requiredHeader}");
            }
        }

        $signature = trim(str_ireplace('Signature=', '', $authHeaderPieces[3]));

        if (!$signature) {
            throw new AuthException("The authorization header signature was not provided");
        }

        $validAuthorizationHeaderParts['algorithm'] = $algorithm;
        $validAuthorizationHeaderParts['credential_parts'] = $credentialParts;
        $validAuthorizationHeaderParts['signed_headers_list'] = $signedHeaderParts;
        $validAuthorizationHeaderParts['signature'] = $signature;

        return $validAuthorizationHeaderParts;
    }

    /**
     * Get public key from the request header
     *
     * @return string Apps public key
     */
    public static function getPublicKey()
    {
        return self::$_parsedRequestHeaders['parsed-x-authorization']['public_key'];
    }

    /**
     * Get the authentication signature from the X-Authorization request header
     *
     * @return string Authenticaton signature digest from request header
     */
    protected function _getSignature()
    {
        SimpleLogger::debug("HmacRequestAuthenticator::_getSignature --- Server HMAC Auth Validate: client sent auth header signatue: '" . self::$_parsedRequestHeaders['parsed-x-authorization']['signature'] . "'");

        return self::$_parsedRequestHeaders['parsed-x-authorization']['signature'];
    }

    /**
     * Calculate a system signature digest to be compared with the signarure digest
     * in the X-Authorization header
     *
     * @return string Calculated signature digest
     */
    protected function _calculateSignature()
    {

        $calculatedSignature = $this->_authenticationTool->generateSignature();

        SimpleLogger::debug("HmacRequestAuthenticator::_calculateSignature --- Server HMAC Auth Validate: Signature digest has been calculated: '{$calculatedSignature}'");

        return $calculatedSignature;
    }

    /**
     * Validate signature. Determines if the requester has demonstrated possession
     * of the correct secret key.
     *
     * Determine if the calculated request signature matches the signature included
     * in the X-Authorization request header. If the signatures match then the requester
     * has demonstrated that they have the correct secret key.
     *
     * @throws AuthException If the authentication credentials are not valid, else does nothing
     */
    protected function _validateSignature()
    {

        SimpleLogger::debug("HmacRequestAuthenticator::_validateSignature --- Server HMAC Auth Validate: Validating signature...");

        $authenticationHeaderSignature = $this->_getSignature();
        $calculatedSignature = $this->_calculateSignature();

        if ($authenticationHeaderSignature !== $calculatedSignature) {
            SimpleLogger::debug("HmacRequestAuthenticator::_validateSignature --- Server HMAC Auth Validate: Signature digests do not match! [ {$authenticationHeaderSignature} / {$calculatedSignature} ]");

            throw new AuthException('The authorization credentials are invalid');
        }

        SimpleLogger::debug("HmacRequestAuthenticator::_validateSignature --- Server HMAC Auth Validate: Signature valid.");
    }

    /**
     * Determine if the authorizaton request has expired
     *
     * If the date/time has expired then reject the request
     *
     * @throws AuthException If the signature header has expired
     */
    protected function _validateAuthorizationHeaderTimeStamp()
    {

        SimpleLogger::debug("HmacRequestAuthenticator::_validateAuthorizationHeaderTimeStamp --- Server HMAC Auth Validate: Validating authorization header timestamp...");

        $authHeaderDate = self::$_parsedRequestHeaders['parsed-x-authorization']['date']; //format: 20150613

        $year = substr($authHeaderDate, 0, 4);
        $month = substr($authHeaderDate, 4, 2);
        $day = substr($authHeaderDate, 6, 2);

        $iso8601Formatted = "{$year}-{$month}-{$day}T00:00:00+00:00"; //format ISO8601: '2014-06-15T15:58:53+00:00'

        if ($this->_isTimestampExpired($iso8601Formatted, self::$_authorizatonHeaderExpiresAfter)) {
            SimpleLogger::debug("HmacRequestAuthenticator::_validateAuthorizationHeaderTimeStamp --- Server HMAC Auth Validate: The authorization header has expired.");
            
            throw new AuthException('Request authorization has expired');
        }

        SimpleLogger::debug("HmacRequestAuthenticator::_validateAuthorizationHeaderTimeStamp --- Server HMAC Auth Validate: Authorization header timestamp validated");
    }

    /**
     * Determine if request has expired
     *
     * If the date/time has expired then reject the request
     *
     * @throws AuthException If the X-Date header has expired
     */
    protected function _validateRequestTimestamp()
    {

        SimpleLogger::debug("HmacRequestAuthenticator::_validateRequestTimestamp --- Server HMAC Auth Validate: Validating request timestamp...");

        $dateHeader = 'x-'. self::$_headerNamespace .'-date';

        if ($this->_isTimestampExpired(self::$_parsedRequestHeaders['header_map'][$dateHeader], self::$_requestExpiresAfter)) {
            SimpleLogger::debug("HmacRequestAuthenticator::_validateRequestTimestamp --- Server HMAC Auth Validate: The request has expired.");
            
            throw new AuthException('Request authorization has expired');
        }

        SimpleLogger::debug("HmacRequestAuthenticator::_validateRequestTimestamp --- Server HMAC Auth Validate: Request timestamp validated");
    }

    /**
     * Determine if request has expired
     *
     * If the date/time has expired then reject the request
     *
     * @param string $timestamp Current timestamp in ISO8601 format
     * @param string $expiresAfter Number of minutes after which the timestamp is expired
     * @return boolean Return true if the time stamp has expired, else false
     */
    protected function _isTimestampExpired($timestamp, $expiresAfter)
    {

        $gmDate = new \DateTime($timestamp);
        $requestGmTimeStamp = $gmDate->getTimestamp();
        $currentGmTimeStamp = time();
        $diffInMinutes = ( abs($currentGmTimeStamp - $requestGmTimeStamp) ) / 60;

        return ($diffInMinutes > $expiresAfter) ? true : false;
    }

    /**
     * Authenticate request:
     * - Perform Message Integrity Check
     * - Validate Signature
     * - Validate time; ensure that request timestamp has not expired
     *
     * @throws AuthException If the authentication credentials are not valid, else does nothing
     */
    public function authenticate()
    {

        SimpleLogger::debug("HmacRequestAuthenticator::authenticate --- Server HMAC Auth Validate: Authenticating...");

        $this->_validateSignature();

        $this->_validateRequestTimestamp();

        $this->_validateAuthorizationHeaderTimeStamp();

        SimpleLogger::debug("HmacRequestAuthenticator::authenticate --- Server HMAC Auth Validate: Authorization valid.");

        return true;
    }
}
