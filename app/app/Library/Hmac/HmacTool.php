<?php
declare(strict_types=1);

namespace App\Library\Hmac;

use App\Library\Hmac\Exceptions\HmacToolException;
use App\Library\Hmac\SimpleLogger;
use App\Library\Hmac\RequestUtils;

/**
 *
 * - Tools for client to generate HMAC (Hashed Message Authentication Code) authentication headers
 * - Tool for a server to generate a HMAC signature digest
 *
 * --- Example 1: Default Headers ---
 *
 * $settings['public_key'] = '12345';
 * $settings['secret_key'] = 'abcdefg';
 * $settings['resource_url'] = 'https://sso.cloud.com/v1/api_client/cool_service/cool_data?param1=hello&param2=world';
 *
 * $authHeaderObj = new HmacTool($settings);
 *
 * $curlHeaders = $authHeaderObj->renderAuthorizationHeaders();
 *
 * Add headers to CURL request:
 * curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
 *
 * --- Example 2: Provide Headers and payload ---
 *
 * $settings['public_key'] = '12345';
 * $settings['secret_key'] = 'abcdefg';
 * $settings['resource_url'] = 'https://sso.cloud.com/v1/api_client/cool_service/cool_data';
 *
 * $settings['http_method'] = 'POST';
 * $settings['hmac_http_headers'] = array('x-tcd-custom: some_value', 'x-tcd-date: 2014-06-15T15:58:53+0000', 'Content-Type: application/x-www-form-urlencoded');
 * $settings['payload'] = 'param1=hello&param2=world';
 *
 * $authHeaderObj = new HmacTool($settings);
 *
 * $curlHeaders = $authHeaderObj->renderAuthorizationHeaders();
 *
 * Add headers to CURL request:
 * curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);
 *
  * --- Example 3: Provide all settings ---
 *
 * $settings['public_key'] = '12345';
 * $settings['secret_key'] = 'abcdefg';
 * $settings['resource_url'] = 'https://sso.cloud.com/v1/api_client/cool_service/cool_data';
 *
 * $settings['algorithm'] = 'TCD-HMAC-SHA256';
 * $settings['http_method'] = 'POST';
 * $settings['hmac_http_headers'] = array('x-tcd-custom: some_value', 'x-tcd-date: 2014-06-15T15:58:53+0000', 'Content-Type: application/x-www-form-urlencoded');
 * $settings['payload'] = 'param1=hello&param2=world';
 *
 * $authHeaderObj = new HmacTool($settings);
 *
 * $curlHeaders = $authHeaderObj->renderAuthorizationHeaders();
 *
 * Add headers to CURL request:
 * curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeaders);*
 */

class HmacTool
{
    protected $_headerNamespace;
    protected $_algorithmVersion;

    protected $_publicKey;
    protected $_secretKey;
    protected $_requestUrlData;
    protected $_parsedRequestHeaders;

    protected $_requestElements;
    protected $_signatureElements;

    protected $_requiredSettingsKeys;
    protected $_requiredRequestElementHeaders;

    protected $_signingKeyHashCache;

    /**
     * Use this string as the payload if the payload is empty
     */
    const DEFAULT_PAYLOAD = 'e3b0c44298fc1c149afbf4c893tm31nnow1e41e4649b934ca495991b7852b855';

    /**
     * Prepare the object
     *
     * Required Settings:
     * 'public_key' = client identifier
     * 'secret_key' = client secret
     * 'resource_url' = FQDN and resource path inlcuding query string, if any
     *
     * Optional Settings:
     * 'payload' = Payload in plain text. Default: self::DEFAULT_PAYLOAD
     * 'http_method' = GET, POST, PUT, or DELETE. Default: 'POST' if 'payload' setting was provided, else 'GET'
     * 'hmac_http_headers' = php curl style header, e.g.: array('<header_name>: <header_value>', '<header_name>: <header_value>'). Default: empty array
     * 'algorithm' = The HMAC algorithm being used. Default: TCD-HMAC-SHA256
     *
     * @see http://php.net/manual/en/function.curl-setopt.php
     * @param string $settings Settings for building the header
     */
    public function __construct($settings)
    {
        SimpleLogger::debug('HmacTool::__construct --- Client HMAC Auth Request: Initializing...');

        RequestUtils::init();

        //validate settings
        $this->_requiredSettingsKeys = array('public_key', 'secret_key', 'resource_url');
        $this->_validateSettings($settings);

        $this->_algorithmVersion = isset($settings['algorithm']) ? $settings['algorithm'] : 'TCD-HMAC-SHA256';

        //save header name space and algo data
        $this->_headerNamespace = strtolower(substr($this->_algorithmVersion, 0, strpos($this->_algorithmVersion, '-')));

        //save public and private key
        $this->_publicKey = $settings['public_key'];
        $this->_secretKey = $settings['secret_key'];

        //get elements needed to render string to sign
        $this->_requiredRequestElementHeaders = array('host', "x-{$this->_headerNamespace}-date");
        $this->_setRequestElements($settings);

        //signing key cache
        $this->_signingKeyHashCache['cache'] = array();
        $this->_signingKeyHashCache['max_cache_size'] = 50;
        $this->_signingKeyHashCache['cache_size'] = 0;

        SimpleLogger::debug('HmacTool::__construct --- Client HMAC Auth Request: Initialized.');
    }

    /**
     * Validate that the required settings were provided.
     * @throws \TriNetCloud\Common\Exception\Hmac\HmacToolException
     */
    protected function _validateSettings($settings)
    {
        $settings = array_filter($settings); //remove empty values
        $missingKeys = RequestUtils::getMissingArrayKeys($settings, $this->_requiredSettingsKeys);

        if ($missingKeys) {
            SimpleLogger::debug("HmacTool::_validateSettings --- The HmacTool is missing required settings:\n".json_encode($missingKeys));

            throw new HmacToolException("The HmacTool is missing required settings:\n".addslashes(json_encode(array_values($missingKeys))));
        }

        if (!filter_var($settings['resource_url'], FILTER_SANITIZE_URL)) {
            SimpleLogger::debug("HmacTool::_validateSettings --- Invalid url: {$settings['resource_url']}");

            throw new HmacToolException("Invalid url: {$settings['resource_url']}");
        }

        if (isset($settings['algorithm']) && preg_match('/[^-]+(?:-HMAC-SHA256)/i', $settings['algorithm']) !== 1) {
            SimpleLogger::debug('HmacTool::_validateSettings --- The provided HMAC algorithm is invalid.');

            throw new HmacToolException("The provided HMAC algorithm is invalid");
        }
    }

    /**
     * Set the elements of the HTTP request that will be used to create the hashed request, which is part of the string to sign
     *
     * Used to build the Canonical Request
     *
     * @param string $settings Settings for the request
     */
    protected function _setRequestElements($settings)
    {
        //parse url data
        $this->_requestUrlData['url'] = $settings['resource_url'];
        $this->_requestUrlData['parsed_url'] = parse_url($settings['resource_url']);
        $this->_requestUrlData['host'] = isset($this->_requestUrlData['parsed_url']['host']) ? $this->_requestUrlData['parsed_url']['host'] : '';

        $requestHeaders = ( isset($settings['hmac_http_headers']) ) ? $settings['hmac_http_headers'] : array();
        $payload = ( isset($settings['payload']) && !empty($settings['payload']) ) ? $settings['payload'] : self::DEFAULT_PAYLOAD;
        $defaultHttpMethod = ( isset($settings['payload']) && !empty($settings['payload']) ) ? 'POST' : 'GET';

        //used to create the hashed request, which is in turn used to create the string to sign
        //all $this->_requestElements elements are derived from passed $settings
        $this->_requestElements['core']['http_method'] = isset($settings['http_method']) && !empty($settings['http_method']) ? $settings['http_method'] : $defaultHttpMethod;
        $this->_requestElements['core']['resource_path'] = isset($this->_requestUrlData['parsed_url']['path']) ? $this->_requestUrlData['parsed_url']['path'] : '';
        $this->_requestElements['core']['query_string'] = isset($this->_requestUrlData['parsed_url']['query']) ? $this->_requestUrlData['parsed_url']['query'] : '';
        $this->_requestElements['core']['headers'] = $this->_parseRequestHeaders($requestHeaders);
        $this->_requestElements['core']['header_list'] = RequestUtils::getCanonicalizedHeaderList($this->_requestElements['core']['headers'], $this->_headerNamespace);
        $this->_requestElements['core']['hashed_payload'] = hash('sha256', (string) $payload);

        $this->_requestElements['request_long_date'] = $this->_parsedRequestHeaders["x-{$this->_headerNamespace}-date"]; //ISO8601 format: 2014-06-15T15:58:53+00:00
        $this->_requestElements['request_short_date'] = str_replace('-', '', substr($this->_requestElements['request_long_date'], 0, 10)); // YYYYMMDD format
        $this->_requestElements['requested_service'] = $this->_getRequestedService($settings['resource_url']);

        $this->_setSignatureElements();

        $this->_validateRequestElements();

        SimpleLogger::debug('HmacTool::_setRequestElements --- Client HMAC Auth Request: Signature elements set');
    }

    /**
     * Validate that the required request elements were provided.
     */
    protected function _validateRequestElements()
    {
        //a host is required
        if (!$this->_requestUrlData['host']) {
            SimpleLogger::debug("HmacTool::_validateRequestElements --- The host was not included in the request url: {$this->_requestUrlData['url']}");

            throw new HmacToolException("The host was not included in the request url: {$this->_requestUrlData['url']}");
        }

        //a resource path is required
        if (!$this->_requestElements['core']['resource_path']) {
            SimpleLogger::debug("HmacTool::_validateRequestElements --- A resource path was not included in the request url: {$this->_requestUrlData['url']}");

            throw new HmacToolException("A resource path was not included in the request url: {$this->_requestUrlData['url']}");
        }

        $missingHeaders = RequestUtils::getMissingArrayKeys($this->_parsedRequestHeaders, $this->_requiredRequestElementHeaders);

        if ($missingHeaders) {
            SimpleLogger::debug("HmacTool::_validateRequestElements --- Required headers are missing: ".json_encode($missingHeaders));

            throw new HmacToolException("Required headers are missing: ".addslashes(json_encode(array_values($missingHeaders))));
        }
    }

    /**
     * Make sure that required headers are in the passed headers
     *
     * Adds missing 'host' and 'x-tcd-date' headers if necessary.
     *
     * @see http://php.net/manual/en/function.curl-setopt.php
     * @param array $headers Array of php curl style headers, e.g.: array('<header_name>: <header_value>', '<header_name>: <header_value>')
     * @return array Array of headers
     */
    protected function _parseRequestHeaders(array $headers = array())
    {
        SimpleLogger::debug("HmacTool::_parseRequestHeaders --- parse: ".json_encode($headers));

        //identify missing headers
        $parsedRequestHeaders = RequestUtils::parseHeaderIndexedArrayToAssocArray($headers);
        $missingHeaderKeys = RequestUtils::getMissingArrayKeys($parsedRequestHeaders, $this->_requiredRequestElementHeaders);

        SimpleLogger::debug("HmacTool::_parseRequestHeaders --- parse into map results: ".json_encode($parsedRequestHeaders));

        if (in_array('host', $missingHeaderKeys)) {
        //add missing host header

            $headers[] = "host: {$this->_requestUrlData['host']}";
        }

        if (in_array("x-{$this->_headerNamespace}-date", $missingHeaderKeys)) {
        //add missing custom date header

            $longDate = gmdate(\DateTime::ATOM, time()); //ISO8601 format: 2014-06-15T15:58:53+00:00

            $headers[] = "x-{$this->_headerNamespace}-date: {$longDate}";
        }

        SimpleLogger::debug("HmacTool::_parseRequestHeaders --- parse results: ".json_encode($headers));

        //save parsed headers to object state
        $this->_parsedRequestHeaders = RequestUtils::parseHeaderIndexedArrayToAssocArray($headers);

        return $headers;
    }

    /**
     * Get the name of the service that is being requested by parsing the resource url that was passed
     *
     * @param string The URL being requested
     * @return string The service
     */
    protected function _getRequestedService($requestUrl)
    {
        $resourcePath = $this->_requestElements['core']['resource_path'];

        $cleanUrlRegExps = '/\/v[0-9]+/'; //remove version e.g.: '/v2/'
        $cleanResourcePath = preg_replace($cleanUrlRegExps, '', $resourcePath);

        $resourcePathSections = explode('/', trim($cleanResourcePath, '/'));

        // "/resource/action" and "/resource" return "resource"
        if (count($resourcePathSections) <= 2) {
            return $resourcePathSections[0];
        }

        // "/controller/action/parameter/" return "action"
        if (count($resourcePathSections) > 2) {
            return $resourcePathSections[1];
        }
    }

    /**
     * Update the object state with the signature elements
     *
     * @return array Formatted headers as an array, or an empty array if no headers were passed
     */
    protected function _setSignatureElements()
    {
        $this->_signatureElements = array();
        $this->_signatureElements['algo_version'] = $this->_algorithmVersion; //algo version in X-Authorization header
        $this->_signatureElements['request_date'] = $this->_requestElements['request_long_date']; //x-tcd-date value
        $this->_signatureElements['credential_scope'] = $this->_getCredentialScope(); //remove (<public key> + '/') from "Credential" param of X-Authorization
        $this->_signatureElements['hashed_request'] = $this->_getHashedRequest();
    }

    /**
     * Get scope of the credentials
     *
     * Format: <yyyymmdd>/<service_name>/<header_namespace>_request
     * e.g.: 20150708/passport_employee_api/tcd_request
     *
     * @return string The scope
     */
    protected function _getCredentialScope()
    {
        return $this->_requestElements['request_short_date'] .
            '/' . $this->_requestElements['requested_service'] .
            '/' . "{$this->_headerNamespace}_request";
    }

    /**
     * Calculate the hashed request based on the request elements
     *
     * @return string Hashed request
     */
    protected function _getHashedRequest()
    {
        $requestString = strtoupper($this->_requestElements['core']['http_method']) . "\n" .
            RequestUtils::getCanonicalizedResourceUrl($this->_requestElements['core']['resource_path']) . "\n" .
            RequestUtils::getCanonicalizedQueryString($this->_requestElements['core']['query_string']) . "\n" .
            RequestUtils::getCanonicalizedHeaders($this->_requestElements['core']['headers'], $this->_headerNamespace) . "\n" .
            $this->_requestElements['core']['header_list']. "\n" .
            $this->_requestElements['core']['hashed_payload'] . "\n";

        SimpleLogger::debug("HmacTool::_getHashedRequest --- HMAC Canonical Request:\n{$requestString}");

        return hash('sha256', $requestString);
    }

    /**
     * Generate the key that will be used to sign the string
     *
     * @return string Signing string that will be used to sign the signing input string.
     * @throws Exception If required signature elements have not been set
     */
    protected function _generateSigningKey()
    {
        $cacheKey = "{$this->_requestElements['request_short_date']}_{$this->_requestElements['requested_service']}_{$this->_secretKey}";

        // Retrieve the hash form the cache or create it and add it to the cache
        if (!isset($this->_signingKeyHashCache['cache'][$cacheKey])) {
            // When the cache size reaches the max, then just clear the cache
            if (++$this->_signingKeyHashCache['cache_size'] > $this->_signingKeyHashCache['max_cache_size']) {
                $this->_signingKeyHashCache['cache'] = array();
                $this->_signingKeyHashCache['cache_size'] = 0;
            }

            $outputRawBinary = true;

            $dateKey = hash_hmac(
                'sha256',
                $this->_requestElements['request_short_date'],
                strtoupper($this->_headerNamespace) . $this->_secretKey,
                $outputRawBinary
            );

            $dateServiceKey = hash_hmac(
                'sha256',
                $this->_requestElements['requested_service'],
                $dateKey,
                $outputRawBinary
            );

            $signingKey = hash_hmac(
                'sha256',
                "{$this->_headerNamespace}_request",
                $dateServiceKey,
                $outputRawBinary
            );

            $this->_signingKeyHashCache['cache'][$cacheKey] = $signingKey;
        }

        SimpleLogger::debug("HmacTool::_generateSigningKey --- HMAC Signing key:\n{$signingKey}");

        return $this->_signingKeyHashCache['cache'][$cacheKey];
    }

    /**
     * Get the string that will be signed for HMAC authentication
     *
     * @return string Signing string that will be signed and used for authentication
     * @throws Exception If required signature elements have not been set
     */
    protected function _generateStringToSign()
    {
        $stringToSign = implode("\n", $this->_signatureElements);

        SimpleLogger::debug("HmacTool::_generateStringToSign --- HMAC String to sign:\n{$stringToSign}");

        return $stringToSign;
    }

    /**
     * Sign the signature string using the secret key
     * by calculating the RFC 2104 HMAC-SHA1 of the signing string
     *
     * @return string Authentication signature
     */
    public function generateSignature()
    {
        $signingKey = $this->_generateSigningKey();
        $stringToSign = $this->_generateStringToSign();

        $signature = hash_hmac('sha256', $stringToSign, $signingKey);

        SimpleLogger::debug("HmacTool::generateSignature --- HMAC signature:\n{$signature}");

        return $signature;
    }

    /**
     * Create a date header string
     * This can be used to set the date header in a CURL request, for example
     * Uses current object state for the date value
     *
     * @return string X-Date header value
     * @throws Exception If required signature elements have not been set
     */
    public function renderDateHeader()
    {
        SimpleLogger::debug('HmacTool::renderDateHeader --- Client HMAC Auth Request: X-Date header rendered');

        return "x-{$this->_headerNamespace}-date: {$this->_requestElements['request_long_date']}";
    }

    /**
     * Create an Authorization header string
     * This can be used to set the Authorization header in a CURL request, for example
     * Uses current object state
     *
     * @return string X-Authorization header value
     * @throws Exception If required signature elements have not been set
     */
    public function renderAuthorizationHeader()
    {
        $credentialScope = $this->_getCredentialScope();
        $credential = $this->_publicKey . '/' . $credentialScope;
        $signedHeaders = $this->_requestElements['core']['header_list'];
        $signature = $this->generateSignature();

        $authorizationHeader = "X-Authorization: {$this->_algorithmVersion} " .
            "Credential={$credential}, " .
            "SignedHeaders={$signedHeaders}, " .
            "Signature={$signature}";

        SimpleLogger::debug("HmacTool::renderAuthorizationHeader --- Client HMAC Auth Request: X-Authorization header rendered:\n{$authorizationHeader}");

        return $authorizationHeader;
    }

    /**
     * Return indexed array of authorization headers
     *
     * @return string All authorization headers in addition to custom headers
     */
    public function renderAuthorizationHeaders()
    {
        $buildAuthenticationHeaders = array();

        //get generated hmac headers
        $buildAuthenticationHeaders[] = $this->renderAuthorizationHeader();

        //add headers provided by user
        $authenticationHeaders = array_merge($buildAuthenticationHeaders, $this->_requestElements['core']['headers']);

        return $authenticationHeaders;
    }
}
