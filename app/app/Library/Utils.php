<?php
declare(strict_types=1);

namespace App\Library;

use Carbon\Carbon;
use DateTimeZone;

class Utils
{
    /**
     * from date time in timezone server to user timezone
     * Note: in composer
     *  "require": {
     *       "nesbot/carbon": "~1.20"
     *   }
     * @param string $dateTime : 2015-01-07 ,...
     * @param int $offset
     * @return string
     * Example: 2018-12-22 11:07:01
     */
    public static function convertDateTimeByOffset($dateTime, $offset)
    {
        $userTimeZone = timezone_name_from_abbr("", $offset * 3600, 0);
        if (empty($userTimeZone)) {
            return $dateTime;
        }
        $dateTimeServer = new Carbon($dateTime);
        // change to user timezone
        $dateTimeServer->setTimezone(new DateTimeZone($userTimeZone));

        return $dateTimeServer->format('Y-m-d H:i:s');
    }

    /**
     * Create a new, pretty (as in moderately, not beautiful That can't be guaranteed ;-) random client secret
     *
     * @return string
     */
    public function newClientSecret()
    {
        $length = 40;
        $chars = '@#!%*+/-=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $str = '';

        $count = strlen($chars);
        while ($length--) {
            $str .= $chars[mt_rand(0, $count - 1)];
        }

        return Hash::make($str);
    }
}
