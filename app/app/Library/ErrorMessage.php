<?php
declare(strict_types=1);

namespace App\Library;

class ErrorMessage
{
    const HTTP_CODE_OK = 200;

    const HTTP_CODE_BAD_REQUEST = 400;

    const HTTP_CODE_UNAUTHORIZED = 401;

    const HTTP_CODE_FORBIDDEN = 403;

    const HTTP_CODE_NOT_FOUND = 404;

    const HTTP_CODE_METHOD_NOT_ALLOWED = 405;

    const HTTP_CODE_INTERNAL_ERROR = 500;

    const HTTP_CODE_NOT_IMPLEMENTED = 501;

    const HTTP_CODE_SERVICE_UNAVAILABLE = 503;

    public static function badRequest()
    {
        return [
            'errorData' => [
                'message' => 'Bad request',
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_BAD_REQUEST
        ];
    }

    public static function unauthorized()
    {
        return [
            'errorData' => [
                'message' => 'Unauthorized',
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_UNAUTHORIZED
        ];
    }

    public static function forbidden()
    {
        return [
            'errorData' => [
                'message' => 'Forbidden',
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_FORBIDDEN
        ];
    }

    public static function notFound()
    {
        return [
            'errorData' => [
                'message' => 'Not Found',
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_NOT_FOUND
        ];
    }

    public static function internalServerError()
    {
        return [
            'errorData' => [
                'message' => 'Internal server error',
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_INTERNAL_ERROR
        ];
    }

    public static function identityUuidNotSent()
    {
        $response = [
            'errorData' => [
                'message' => "The Identity Uuid is missing from the request header.",
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_BAD_REQUEST
        ];
        
        return $response;
    }

    public static function clientAppIdentifierNotSent()
    {
        $response = [
            'errorData' => [
                'message' => "The Identity Uuid is missing from the request header.",
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_BAD_REQUEST
        ];
        
        return $response;
    }
    
    public static function supported3LaOnly()
    {
        $response = [
            'errorData' => [
                'message' => "This endpoint only support 3LA authentication",
                'data' => null
            ],
            'httpCode' => self::HTTP_CODE_BAD_REQUEST
        ];
    
        return $response;
    }
}
