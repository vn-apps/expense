<?php
declare(strict_types=1);

namespace App\Library;

class Messages
{
    const OK = 1;
    const FAILURE = 0;
    const OK_MSG = 'Successful';
}
