<?php
declare(strict_types=1);

namespace App\Exceptions;

use App\Library\ErrorMessage;
use App\Serializers\DataSerializer;
use App\Transformers\ErrorTransformer;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @var ErrorTransformer 
     */
    private $errorTransformer;

    /**
     * @var TrinetDataSerializer
     */
    private $serializer;

    public function __construct(Container $container, ErrorTransformer $errorTransformer, DataSerializer $serializer)
    {
        parent::__construct($container);

        $this->errorTransformer = $errorTransformer;
        $this->serializer = $serializer;
    }

    /**
     * Report or log an exception.
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request $request
     * @param  \Exception               $exception
     * @return Response
     */
    public function render($request, Exception $ex) //$exception
    {
        $statusCode = ErrorMessage::HTTP_CODE_UNAUTHORIZED;
        $errorMessage = $ex->getMessage();

        if ($ex instanceof ApiHttpException) {
            $atts = $ex->getAttributes();
            $statusCode = $atts['statusCode'];
            $errorMessage = $atts['message'];
        } elseif ($ex instanceof HttpException) {
            $statusCode = $ex->getStatusCode();
        } elseif ($ex instanceof ValidationException) {
            $statusCode = 422;
            $errorMessage = $ex->validator->getMessageBag();
        }

        return fractal()
            ->addMeta(['code' => 0, 'message' => $errorMessage])
            ->transformWith($this->errorTransformer)
            ->serializeWith($this->serializer)
            ->respond($statusCode);
    }

    // /**
    //  * Convert an authentication exception into an unauthenticated response.
    //  *
    //  * @param  \Illuminate\Http\Request $request
    //  * @param  \Illuminate\Auth\AuthenticationException  $exception
    //  * @return \Illuminate\Http\Response
    //  */
    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    //     if ($request->expectsJson()) {
    //         return response()->json(['error' => 'Unauthenticated.'], 401);
    //     }

    //     return redirect()->guest(route('login'));
    // }
}
