<?php
declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use function GuzzleHttp\json_encode;
use Exception;

class ApiHttpException extends HttpException
{
    /**
     * Array of attributes that are passed in from the constructor, and
     * made available in the view when a development error is displayed.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Constructor.
     *
     * @param string          $statusCode
     * @param string|array    $message    Either the string of the error message, or an array of attributes
     *                                    that are made available in the view, and sprintf()'d into
     *                                    Exception::$_messageTemplate
     * @param \Exception|null $previous   the previous exception.
     * @param array           $headers
     * @param int             $code       The code of the error, is also the HTTP status code for the error.
     */
    public function __construct(
        $statusCode,
        $message = null,
        Exception $previous = null,
        array $headers = array(),
        $code = 0
    ) {
        if (is_array($message)) {
            $this->attributes = $message;
            $message = json_encode($message);
        }

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    /**
     * Get the passed in attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}
