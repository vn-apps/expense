<?php
declare(strict_types=1);

namespace App\Modules\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required|string',
            'model_name' => 'required',
            'color' => 'required',
            'registration_year' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'brand.required' => 'Please input brand!',
            'model_name.required' => 'Please input model_name',
            'color.required' => 'Please input color',
            'registration_year.required' => 'Please input registration_year'
        ];
    }
}
