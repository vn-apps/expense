<?php
declare(strict_types=1);

namespace App\Modules\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     * @return array
     */
    public function messages()
    {
        return [
            'email.required.email.string' => 'Email input',
            'password' => 'Input password'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => __('app.title'),
            'password' => 'Input password'
        ];
    }
}
