<?php
declare(strict_types=1);

namespace App\Modules\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string',
            'phone' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please input company name!',
            'address.required' => 'Please input address',
            'phone.required' => 'Please enter phone number!'
        ];
    }
}
