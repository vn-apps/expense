<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\VehicleRequest;
use App\Repositories\Interfaces\VehicleRepositoryInterface as VehicleRepository;
use App\Transformers\VehicleTransformer;
use App\Transformers\ErrorTransformer;
use Illuminate\Http\Request;
use App\Serializers\DataSerializer;

class VehiclesController extends Controller
{
    protected $vehicle = null;

    /**
     * Vehicle instance
     * 
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(
        DataSerializer $dataSerializer,
        ErrorTransformer $errorTransformer,
        VehicleRepository $vehicleRepository,
        VehicleTransformer $vehicleTransformer
    ) {
        parent::__construct($dataSerializer, $errorTransformer);

        $this->vehicle = $vehicleRepository;
        $this->transformer = $vehicleTransformer;
    }

    /**
     * @param Request $resuqest
     * @return \Spatie\Fractal\Fractal
     */
    public function index(Request $resuqest)
    {
        $vehicles = $this->vehicle->getListVehicles($resuqest->all());

        return $this->respondPagination($vehicles);
    }

    /**
     * @param VehicleRequest $request
     * @return \Spatie\Fractal\Fractal
     */
    public function create(VehicleRequest $request)
    {
        $vehicleItem = $this->vehicle->create($request->all());

        return $this->respondByItem($vehicleItem);
    }
}
