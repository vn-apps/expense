<?php

declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\CategoryRequest;
use App\Repositories\Interfaces\CategoryRepositoryInterface as CategoryRepository;
use App\Serializers\DataSerializer;
use App\Transformers\CategoryTransformer;
use App\Transformers\ErrorTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class CategoriesController extends Controller {

    protected $cat = null;

    /**
     * Category instance
     * 
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(
            DataSerializer $dataSerializer,
            ErrorTransformer $errorTransformer,
            CategoryRepository $categoryRepository,
            CategoryTransformer $catTransformer
    ) {
        parent::__construct($dataSerializer, $errorTransformer);

        $this->cat = $categoryRepository;
        $this->transformer = $catTransformer;
    }

    /**
     * @param Request $request
     * @return Fractal
     */
    public function index(Request $request) {
        $cats = $this->cat->getListCategories($request->all());

        return $this->respondPagination($cats);
    }

    /**
     * @param  CategoryRequest $request
     * @return Fractal
     */
    public function create(CategoryRequest $request) {

        $catItem = $this->cat->create($request->all());

        return $this->respondByItem($catItem);
    }

}
