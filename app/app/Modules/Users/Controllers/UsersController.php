<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\UserRequest;
use App\Repositories\Interfaces\UserRepositoryInterface as UserRepository;
use App\Serializers\DataSerializer;
use App\Transformers\ErrorTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class UsersController extends Controller
{
    /* @var UserRepository */
    protected $user;

    /*@var */
    protected $userTransformer;

    /**
     * Users instance
     * 
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(
        DataSerializer $dataSerializer,
        ErrorTransformer $errorTransformer,
        UserRepository $userRepository,
        UserTransformer $userTransformer
    ) {
        parent::__construct($dataSerializer, $errorTransformer);

        $this->user = $userRepository;
        $this->transformer = $userTransformer;
    }

    /**
     * Get List users
     * 
     * @param  Request $request
     * @return Fractal
     */
    public function index(Request $request)
    {
        $perPage = $request->get('per_page') ?? 20;
        $users = $this->user->getListUsers($perPage);

        return $this->respondPagination($users);
    }

    /**
     * @param  UserRequest $request
     * @return Fractal
     */
    public function create(UserRequest $request)
    {
        $result = $this->user->create($request->all());

        return $this->respondByItem($result);
    }
}
