<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\LoginRequest;
use App\Modules\Users\Requests\UserRequest;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\Users\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @param UserRequest $request
     * @param UserTransformer $userTransformer
     * @return fractal
     */
    public function signup(
            UserRequest $request,
            UserTransformer $userTransformer,
            UserRepositoryInterface $user
    ) {
        $userData = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'type' => $request->get('type'),
            'role' => $request->get('role'),
            'avatar' => 'ss.jpg',
            'status' => 0
        ];

        $result = $user->create($userData);

        if (empty($result)) {
            return $this->errorResponse('Sorry! Registration is not successfull.');
        }

        // $result->token = $result->createToken('token')->accessToken;

        return fractal($result, $userTransformer, $this->dataSerializer)->respond();
    }

    /**
     * @param LoginRequest $request
     * @return fractal
     */
    public function signin(
            LoginRequest $request,
            UserTransformer $userTransformer
    ) {

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            return $this->errorResponse('Unauthorized!');
        }

        $user = $request->user();
        $user->token = $user->createToken('token')->accessToken;

        return fractal($user, $userTransformer, $this->dataSerializer)->respond();
    }

    /**
     * @param  Request $request
     * @return fractal
     */
    public function logout(Request $request, UserTransformer $userTransformer) {
        $user = $request->user();
        $isUser = $user->token()->revoke();
        if (empty($isUser)) {
            return $this->errorResponse('Something went wrong!');
        }
        echo "Successfully logged out.";
        exit;

        //return fractal($user, $userTransformer, $this->dataSerializer)->respond();
    }

    // //getuser
    // public function getUser(Request $request)
    // {
    //     //$id = $request->user()->id;
    //     $user = $request->user();
    //     if ($user) {
    //         return $this->sendResponse($user);
    //     } else {
    //         $error = "user not found";
    //         return $this->sendResponse($error);
    //     }
    // }
}
