<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\BookingRequest;
use App\Repositories\Interfaces\BookingRepositoryInterface as BookingRepository;
use App\Serializers\DataSerializer;
use App\Transformers\BookingTransformer;
use App\Transformers\ErrorTransformer;
use App\Transformers\UserTransformer;

class BookingController extends Controller
{
    /* @var BookingRepository */
    protected $booking = null;

    /**
     * Users instance
     * 
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(
        DataSerializer $dataSerializer,
        ErrorTransformer $errorTransformer,
        BookingRepository $bookingRepository,
        UserTransformer $userTransformer
    ) {
        parent::__construct($dataSerializer, $errorTransformer);

        $this->booking = $bookingRepository;
        $this->transformer = $userTransformer;
    }

    /**
     * @param BookingTransformer $bookingTransformer
     */
    public function index(BookingTransformer $bookingTransformer)
    {
        $bookingList = $this->booking->getListBooking();

        return fractal($bookingList, $bookingTransformer, $this->dataSerializer)->respond();
    }

    /**
     * @param  BookingRequest $request
     * @return fractal
     */
    public function create(BookingRequest $request, BookingTransformer $bookingTransformer)
    {
        $data = [
            'name' => $request->get('name'),
            'destination' => $request->get('destination'),
            'driver_id' => $request->get('driver_id'),
            'vehicle_type' => $request->get('vehicle_type'),
            'note' => $request->get('note'),
            'status' => $request->get('status')
        ];

        $bookingItem = $this->booking->create($data);

        return fractal($bookingItem, $bookingTransformer, $this->dataSerializer)->respond();
    }
}
