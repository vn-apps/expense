<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Requests\CompanyRequest;
use App\Repositories\Interfaces\CompanyRepositoryInterface as CompanyRepository;
use App\Serializers\DataSerializer;
use App\Transformers\CompanyTransformer;
use App\Transformers\ErrorTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class CompaniesController extends Controller
{
    protected $company = null;

    /**
     * Company instance
     * 
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(
        DataSerializer $dataSerializer,
        ErrorTransformer $errorTransformer,
        CompanyRepository $companyRepository,
        CompanyTransformer $companyTransformer
    ) {
        parent::__construct($dataSerializer, $errorTransformer);

        $this->company = $companyRepository;
        $this->transformer = $companyTransformer;
    }
    

    /**
     * @param Request $request
     * @return Fractal
     */
    public function index(Request $request)
    {
        $companies = $this->company->getListCompanies($request->all());

        return $this->respondPagination($companies);
    }

    /**
     * @param CompanyRequest $request
     * @return Fractal
     */
    public function create(CompanyRequest $request)
    {
        $companyItem = $this->company->create($request->all());

        return $this->respondByItem($companyItem);
    }
}
