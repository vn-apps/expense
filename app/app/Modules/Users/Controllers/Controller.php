<?php
declare(strict_types=1);

namespace App\Modules\Users\Controllers;

use App\Exceptions\ApiHttpException;
use App\Serializers\DataSerializer;
use App\Traits\ApiResponser;
use App\Transformers\ErrorTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class Controller extends BaseController
{
    use ApiResponser;

    /**
     * @var TrinetDataSerializer 
     */
    protected $dataSerializer;

    /**
     * @var Request 
     */
    protected $request;

    /**
     * @var ErrorTransformer 
     */
    private $errorTransformer;

    /**
     * @var 
     */
    protected $transformer = null;
    /**
     * Controller constructor.
     *
     * @param DataSerializer   $dataSerializer
     * @param ErrorTransformer $errorTransformer
     */
    public function __construct(DataSerializer $dataSerializer, ErrorTransformer $errorTransformer)
    {
        $this->dataSerializer = $dataSerializer;
        $this->errorTransformer = $errorTransformer;

        $this->transformer = new UserTransformer();
    }

    /**
     * Get Identity UUID from header
     *
     * @return string on Success
     * @throws ApiHttpException if not found
     */
    protected function getIndentityUuidFromHeader()
    {
        //Log::debug("Retrieving Identity UUID from http 'HTTP_X_TCD_IDENTITY_UUID' header...");
        if (isset($_SERVER['HTTP_X_TCD_IDENTITY_UUID']) === false || trim($_SERVER['HTTP_X_TCD_IDENTITY_UUID'] == '')) {
            echo "Errorss"; exit;
            //$error = ErrorMessage::identityUuidNotSent();

            //throw new ApiHttpException($error['httpCode'], $error['errorData']);
        }

        $identityUUID = trim($_SERVER['HTTP_X_TCD_IDENTITY_UUID']);
        //Log::debug("Retrieved identity UUID from http 'HTTP_X_TCD_IDENTITY_UUID' header: {$identityUUID}");

        return $identityUUID;
    }

    /**
     * @param  $errorMessage
     * @param  int $statusCode
     * @return Fractal
     */
    protected function errorResponse($errorMessage, $statusCode = 400)
    {
        return fractal()->addMeta(['code' => 404, 'message' => $errorMessage])
                        ->transformWith($this->errorTransformer)
                        ->serializeWith($this->dataSerializer)
                        ->respond($statusCode);
    }

    /**
     * @param  null|LengthAwarePaginator $data
     * @return fractal
     */
    protected function respondPagination($data)
    {
        return fractal()->collection($data)
                        ->transformWith($this->transformer)
                        ->serializeWith($this->dataSerializer)
                        ->paginateWith(new IlluminatePaginatorAdapter($data));
    }

    /**
     * @param  null|mixed $data
     * @return fractal
     */
    protected function respondByItem($data)
    {
        return fractal($data)->transformWith($this->transformer)
                            ->serializeWith($this->dataSerializer)
                            ->respond();
    }
}
