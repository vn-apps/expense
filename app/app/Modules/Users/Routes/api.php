<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$namespace = '\App\Modules\Users\Controllers';

Route::group(['prefix' => 'v2', 'namespace' => $namespace], function () {
    Route::get('/', function () {
        return response('Api v2', 200)->header('Content-Type', 'text/plain');
    });

    Route::group(['prefix' => 'users', ['middleware' => 'auth:api']], function () {
        Route::get('list', ['as' => 'v2.users.index', 'uses' => 'UsersController@index']);
        Route::post('create', ['as' => 'v2.users.create', 'uses' => 'UsersController@create']);
    });

    Route::group(['prefix' => 'categories',  ['middleware' => 'auth:api']], function () {
        Route::get('list', ['as' => 'v2.categories.index', 'uses' => 'CategoriesController@index']);
        Route::post('create', ['as' => 'v2.categories.create', 'uses' => 'CategoriesController@create']);
    });

    Route::group(['prefix' => 'companies', ['middleware' => 'auth:api']], function () {
        Route::get('list', ['as' => 'v2.companies.index', 'uses' => 'CompaniesController@index']);
        Route::post('create', ['as' => 'v2.companies.create', 'uses' => 'CompaniesController@create']);
    });

    Route::group(['prefix' => 'vehicles', ['middleware' => 'auth:api']], function () {
        Route::get('list', ['as' => 'v2.vehicles.index', 'uses' => 'VehiclesController@index']);
        Route::post('create', ['as' => 'v2.companies.create', 'uses' => 'VehiclesController@create']);
    });

    Route::group(['prefix' => 'booking', ['middleware' => 'auth:api']], function () {
        Route::get('list', ['as' => 'v2.booking.index', 'uses' => 'BookingController@index']);
        Route::post('create', ['as' => 'v2.booking.create', 'uses' => 'BookingController@create']);
    });

    Route::group([ 'prefix' => 'auth'], function () { 
        Route::group(['middleware' => ['guest:api']], function () {
            Route::post('signin', ['as' => 'v2.auth.signin', 'uses' => 'AuthController@signin']);
            Route::post('signup', ['as' => 'v2.auth.signup', 'uses' => 'AuthController@signup']);
        });
        Route::group(['middleware' => 'auth:api'], function() {
            Route::get('logout', ['as' => 'v2.auth.logout', 'uses' => 'AuthController@logout']);
            //Route::get('getuser', ['as' => 'v2.auth.getuser', 'uses' => 'AuthController@getUser']);
        });
    }); 
});
