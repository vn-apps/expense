<?php
$namespace = '\App\Modules\Users\Controllers';

Route::group(['prefix' => 'v2', 'namespace' => $namespace], function () {
    Route::get('/', function () {
        return response('Api v2', 200)
            ->header('Content-Type', 'text/plain');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('list', ['as' => 'v2.users.index', 'uses' => 'UsersController@index']);
    });
});
