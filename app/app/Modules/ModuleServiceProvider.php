<?php

namespace App\Modules;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        $modules = config('module.modules');

        foreach ($modules as $module) {
            if (file_exists(__DIR__ . '/' . $module . '/Routes/api.php')) {
                include __DIR__ . '/' . $module . '/Routes/api.php';
            }

            if (is_dir(__DIR__ . '/' . $module . '/Views')) {
                $this->loadViewsFrom(__DIR__ . '/' . $module . '/Views', $module);
            }
        }
    }
}
