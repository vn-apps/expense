<?php
declare(strict_types=1);

$namespace = '\App\Modules\Tracking\Controllers';

Route::group(['prefix' => 'v2', 'namespace' => $namespace], function () {
    Route::group(['prefix' => 'tracking'], function () {
        Route::get('list', ['as' => 'v2.tracking.index', 'uses' => 'TrackingController@index']);
    });
});
