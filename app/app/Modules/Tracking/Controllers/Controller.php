<?php
declare(strict_types=1);

namespace App\Modules\Tracking\Controllers;

use App\Exceptions\ApiHttpException;
use App\Library\ErrorMessage;
use App\Serializers\DataSerializer;
use App\Traits\ApiResponser;
use App\Transformers\ErrorTransformer;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Spatie\Fractal\Fractal;

class Controller extends BaseController
{
    use ApiResponser;

    /** @var TrinetDataSerializer */
    protected $dataSerializer;

    /** @var Request */
    protected $request;

    /** @var ErrorTransformer */
    private $errorTransformer;

    /**
     * Controller constructor.
     * @param DataSerializer $dataSerializer
     * @param ErrorTransformer $errorTransformer
     * @param Request $request
     */
    public function __construct(DataSerializer $dataSerializer, ErrorTransformer $errorTransformer, Request $request)
    {
        $this->dataSerializer = $dataSerializer;
        $this->errorTransformer = $errorTransformer;
        $this->request = $request;
    }

    /**
     * Get Identity UUID from header
     *
     * @return string on Success
     * @throws ApiHttpException if not found
     */
    protected function getIndentityUuidFromHeader()
    {
        Log::debug("Retrieving Identity UUID from http 'HTTP_X_TCD_IDENTITY_UUID' header...");
        if (isset($_SERVER['HTTP_X_TCD_IDENTITY_UUID']) === false || trim($_SERVER['HTTP_X_TCD_IDENTITY_UUID'] == '')) {
            $error = ErrorMessage::identityUuidNotSent();

            throw new ApiHttpException($error['httpCode'], $error['errorData']);
        }

        $identityUUID = trim($_SERVER['HTTP_X_TCD_IDENTITY_UUID']);
        Log::debug("Retrieved identity UUID from http 'HTTP_X_TCD_IDENTITY_UUID' header: {$identityUUID}");

        return $identityUUID;
    }

    /**
     * @param $errorMessage
     * @param int $statusCode
     * @return Fractal
     */
    protected function errorResponse($errorMessage, $statusCode = 400)
    {
        return fractal()->addMeta(['code' => 404, 'message' => $errorMessage])
                        ->transformWith($this->errorTransformer)
                        ->serializeWith($this->DataSerializer)
                        ->respond($statusCode);
    }
}
