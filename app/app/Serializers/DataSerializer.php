<?php
declare(strict_types=1);

namespace App\Serializers;

use League\Fractal\Pagination\CursorInterface;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Serializer\SerializerAbstract;

class DataSerializer extends SerializerAbstract
{
    /**
     * @var array
     */
    protected $envelope = [
        'code' => 200,
        'message' => 'Successfully',
        'data' => [],
    ];

    /**
     * @param integer $code
     */
    public function setCode($code)
    {
        $this->envelope['code'] = $code;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->envelope['message'] = $message;
    }

    /**
     * @param mixed $metadata
     */
    public function setMetaData($metadata)
    {
        $this->envelope['metadata'] = $metadata;
    }

    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        $this->envelope['data'] = $data;
        return $this->envelope;
    }

    /**
     * Serialize an item.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        $this->envelope['data'] = $data;
        return $this->envelope;
    }

    /**
     * Serialize null resource.
     *
     * @return array
     */
    public function null()
    {
        $this->envelope['data'] = null;
        return $this->envelope;
    }

    /**
     * Serialize the included data.
     *
     * @param ResourceInterface $resource
     * @param array $data
     *
     * @return array
     */
    public function includedData(ResourceInterface $resource, array $data)
    {
        return $data;
    }

    /**
     * Serialize the meta.
     *
     * @param array $meta
     *
     * @return array
     */
    public function meta(array $meta)
    {
        if (empty($meta)) {
            return [];
        }

        if (isset($meta['code'])) {
            $this->setCode($meta['code']);
        }
        if (isset($meta['message'])) {
            $this->setMessage($meta['message']);
        }

        unset($meta['code']);
        unset($meta['message']);
        if (!empty($meta)) {
            $this->setMetaData($meta);
        }

        return $this->envelope;
    }

    /**
     * Serialize the paginator.
     *
     * @param PaginatorInterface $paginator
     *
     * @return array
     */
    public function paginator(PaginatorInterface $paginator)
    {
        $currentPage = (int)$paginator->getCurrentPage();
        $lastPage = (int)$paginator->getLastPage();

        $pagination = [
            'total' => (int)$paginator->getTotal(),
            'count' => (int)$paginator->getCount(),
            'per_page' => (int)$paginator->getPerPage(),
            'current_page' => $currentPage,
            'total_pages' => $lastPage,
        ];

        $pagination['links'] = [];

        if ($currentPage > 1) {
            $pagination['links']['previous'] = $paginator->getUrl($currentPage - 1);
        }

        if ($currentPage < $lastPage) {
            $pagination['links']['next'] = $paginator->getUrl($currentPage + 1);
        }

        $this->envelope['pagination'] = $pagination;
        return $this->envelope;
    }

    /**
     * Serialize the cursor.
     *
     * @param CursorInterface $cursor
     *
     * @return array
     */
    public function cursor(CursorInterface $cursor)
    {
        $cursor = [
            'current' => $cursor->getCurrent(),
            'prev' => $cursor->getPrev(),
            'next' => $cursor->getNext(),
            'count' => (int)$cursor->getCount(),
        ];

        $this->envelope['cursor'] = $cursor;
        return $this->envelope;
    }

    /**
     * set meta to current timestamp in the response.
     * Example:
     * "metadata" : {
            "current_timestamp": < string - ISO8601 format >,
        }
     */
    public function setMetaCurrentTimeStamp()
    {
        $this->setMetaData(['current_timestamp' => date('Y-m-d h:i:s')]);
    }
}
