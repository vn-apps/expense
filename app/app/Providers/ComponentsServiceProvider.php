<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use TriNetExpense\Authorize\AuthorizeACO;

class ComponentsServiceProvider extends ServiceProvider
{
    /** @var bool */
    protected $defer = true;

    public function register()
    {
       
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            //AuthorizeComponent::class
        ];
    }
}
