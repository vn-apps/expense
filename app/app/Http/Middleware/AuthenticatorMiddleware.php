<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Library\Hmac\HmacRequestAuthenticator;
use Exception;
use Illuminate\Http\Request;

class AuthenticatorMiddleware
{
    /**
     * Handle an incoming request.
     * @param Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->authenticate($request)) {
            return response('Forbidden!!', 403)
                    ->header('Content-Type', 'text/plain');
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws HttpException
     */
    protected function authenticate($request)
    {
        //Log::info('Hmac authenticate');
        try {
           // HmacRequestAuthenticator::init();

            // $publicKey = HmacRequestAuthenticator::getPublicKey();
            // $secretKey = env('CLIENT_SECRET');
            // $payload = $request->getContent();

            // $authRequestObj = new HmacRequestAuthenticator($publicKey, $secretKey, $payload);

            // $authRequestObj->authenticate();
           
            return true;
        } catch (\Exception $ex) {
            //Log::error($ex->getMessage());
            echo "Down error Exception";
            //throw new HttpException(403, 'Forbidden');
        }
    }
}
