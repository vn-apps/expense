<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Transformers\CategoryTransformer;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Spatie\Fractal\Fractal;

class CategoriesController extends Controller
{
    /**
     * Get a list categories
     *
     * @param CategoryTransformer $categoryTransformer
     * @param CategoryRepositoryInterface $category
     * @return \Spatie\Fractal\Fractal
     */
    public function index(CategoryTransformer $categoryTransformer, CategoryRepositoryInterface $category)
    {
        $categories = $category->getListCategories();
        
        return fractal($categories, $categoryTransformer, $this->dataSerializer)->respond();
    }

    /**
     * Get a category by id
     *
     * @param CategoryTransformer $categoryTransformer
     * @param CategoryRepository $category
     * @param int $id
     * @return \Spatie\Fractal\Fractal
     */
    public function getCategoryById(CategoryTransformer $categoryTransformer, CategoryRepositoryInterface $category, $id)
    {
        $category = $category->find($id);
       
        return fractal($category, $categoryTransformer, $this->dataSerializer)->respond();
    }

    /**
     * Create a expense
     *
     * @param CategoryTransformer $categoryTransformer
     * @param CategoryRepository $category
     * @return \Spatie\Fractal\Fractal
     * @throws \Exception
     */
    public function create(CategoryTransformer $categoryTransformer, CategoryRepository $category)
    {
        if (!$this->request->isMethod('POST')) {
            return "Invalid!!";
        }
 
        $categoryData = [
            'name' => $this->request->name,
            'parent_id' => $this->request->parent_id,
            'status' => $this->request->status
        ];

        $categoryItem = $category->create($categoryData);

        return fractal($categoryItem, $categoryTransformer, $this->dataSerializer)->respond();
    }

    /**
     * Update Category by id
     *
     * @param CategoryRepository $category
     * @param CategoryTransformer $categoryTransformer
     * @param int $id
     * @return \Spatie\Fractal\Fractal
     */
    public function update(CategoryRepository $category, CategoryTransformer $categoryTransformer, $id)
    {
        if (!$this->request->isMethod('PUT') || empty($id)) {
            return "Invalid";
        }

        $categoryData = [
            'name' => $this->request->name,
            'parent_id' => $this->request->parent_id,
            'status' => $this->request->status 
        ];

        $category->update($categoryData, $id);
        $categoryItem = $category->find($id);

        return fractal($categoryItem, $categoryTransformer, $this->dataSerializer)->respond();
    }

    /**
     * Delete Category by id
     *
     * @param CategoryRepository $category
     * @param CategoryTransformer $categoryTransformer
     * @param int $id
     * @return \Spatie\Fractal\Fractal
     */
    public function delete(CategoryRepository $category, CategoryTransformer $categoryTransformer, $id) {
        if (!$this->request->isMethod('DELETE') || empty($id)) {
            return "Invalid";
        }
      
        $categoryItem = $category->find($id);
        $category->delete($id);

        return fractal($categoryItem, $categoryTransformer, $this->dataSerializer)->respond();
    }
}
