<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Requests\UserRequest;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UsersController extends Controller
{
    /**
     * @param UserTransformer $userTransformer
     * @param UserRepository $user
     * @return \Spatie\Fractal\Fractal
     */
    public function index(UserTransformer $userTransformer, UserRepository $user)
    {
        $users = $user->getListUsers();

        return fractal($users, $userTransformer, $this->dataSerializer)->respond();
    }

    /**
     * Get a user by id
     * @param UserTransformer $userTransformer
     * @param UserRepository $user
     * @param int $id
     * @return \Spatie\Fractal\Fractal
     */
    public function getUserById(UserTransformer $userTransformer, UserRepository $user, int $id)
    {
        echo "sadad"; exit;
        $user = $user->find($id);
       
        return fractal($user, $userTransformer, $this->dataSerializer)->respond();
    }

    /**
     * 
     */
    public function addUser()
    {
        
    }
}

