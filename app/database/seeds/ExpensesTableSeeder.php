<?php

use Illuminate\Database\Seeder;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Test expense 001',
                'description' => 'Description test',
                'category_id' => 1,
                'user_id' => 1,
                'amount' => 0.5
            ],
            [
                'name' => 'Test expense 002',
                'description' => 'Description test',
                'category_id' => 1,
                'user_id' => 1,
                'amount' => 3
            ],
            [
                'name' => 'Test expense 003',
                'description' => 'Description test',
                'category_id' => 1,
                'user_id' => 2,
                'amount' => 2
            ],
            [
                'name' => 'Test expense 004',
                'description' => 'Description test',
                'category_id' => 2,
                'user_id' => 2,
                'amount' => 1
            ],
        ];

        DB::table('expenses')->insert($items);
    }
}
