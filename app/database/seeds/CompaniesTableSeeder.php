<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Company 0001',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Company 002',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Company 003',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Company 004',
                'parent_id' => 0,
                'status' => 1
            ],
        ];

        DB::table('companies')->insert($items);
    }
}
