<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Agent Fee',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Hotel',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Meals',
                'parent_id' => 0,
                'status' => 1
            ],
            [
                'name' => 'Travel',
                'parent_id' => 0,
                'status' => 1
            ],
        ];

        DB::table('categories')->insert($items);
    }
}
