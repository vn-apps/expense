<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'first_name' => 'User 1',
                'last_name' => 'tran',
                'email' => 'user1@gmail.com',
                'password' => md5('123456')
            ],
            [
                'first_name' => 'User 2',
                'last_name' => 'Nguyen',
                'email' => 'user2@gmail.com',
                'password' => md5('123456')
            ]
        ];

        DB::table('users')->insert($items);
    }
}
