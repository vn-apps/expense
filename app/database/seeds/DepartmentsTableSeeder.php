<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Tài xế',
                'parent_id' => 1
            ],
            [
                'name' => 'Quản lý',
                'parent_id' => 1
            ],
        ];

        DB::table('departments')->insert($items);
    }
}
