<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand');
            $table->string('model_name');
            $table->string('license_plate');
            $table->string('color');
            $table->string('registration_year');
            $table->string('description')->nullable();
            $table->string('picture')->nullable();
            $table->tinyInteger('type')->default(0)->nullable();
            $table->boolean('status')->default(0)->comment('0: Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
