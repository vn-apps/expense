<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name')->nullable($value = true);
            $table->string('thumbnail')->nullable($value = true);
            $table->integer('expense_id')->unsigned()->default(0); 
            $table->timestamps();
            $table->index(['expense_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_attachments');
    }
}
