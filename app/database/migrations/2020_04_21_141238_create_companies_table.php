<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('introduction')->nullable();
            $table->string('address')->nullable();
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->string('logo')->nullable();
            $table->integer('parent_id')->default(0);
            $table->boolean('status')->default(1)->comment('0: Active, 1: Inactive');	
            $table->timestamps();

            $table->index(['parent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
