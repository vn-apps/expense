<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->default(0);
            $table->string('first_name', 100);
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 100);
            $table->string('avatar', 50)->nullable();
            $table->tinyInteger('group_id')->default(0);
            $table->tinyInteger('role')->default(0);
            $table->tinyInteger('type')->default(0)->comment('0: User, 1: Driver');
            $table->string('license')->nullable();;
            $table->string('license_valid')->nullable();
            $table->boolean('status')->default(0)->comment('0: Active, Locked, unverified,Disabled, Enabled');
            $table->rememberToken();
            $table->timestamps();

            $table->index('company_id');
            $table->index('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
