Installation Instructions - Php Projects
-------------------------------------------

# Installing via Composer :
1. Require composer command available. Install composer by follow this link: https://getcomposer.org/doc/00-intro.md
2. Modify existing or create new composer.json file in project folder.
3. From project folder run command: composer install


# Environment configuration

1. Application environment configuration
   Copy file .env.example to .env at the same folder
   Modify the configuration for matching the environment app running

2. Testing - Uts environment configuration
   Copy file .env.testing.example to .env.testing at the same folder
   Modify the configuration for matching the environment for executing Uts


# Appendix

1. Run unit-test cases:

    Update the migration script to the latest database

    ```
    rm -f ./database/migrations/*.php && php artisan migrate:generate --no-interaction --defaultIndexNames && php ./vendor/bin/phpunit
    ```

2. Run Coding Standard check

    ```
    php vendor/bin/phpcs --standard=../server/sniffs/codesniffer_custom_ruleset.xml --error-severity=1 --warning-severity=6 --extensions=php -v ./
    ```

3. Run Coding Standard autofix

    ```
    php vendor/bin/phpcbf --standard=../server/sniffs/codesniffer_custom_ruleset.xml --error-severity=1 --warning-severity=6 --extensions=php -v ./
    ```
    
4. Run Duplicate Codes check on code and Uts

    ```
    php vendor/bin/phpcpd --progress ./app ./tests
    ```
    
5. Run Code Coverage check using public/coverage/clover/clover.xml (created by phpunit)

    ```
    php /var/www/expense_api/server/deploy/development/coverageChecker.php tests/coverage/clover.xml /var/www/expense_api/server/deploy/development/coverageCutoff
    ```

6. After create Data Seeder class for run Uts. Please run below command to update auto load class

    ```
    composer dump-autoload
    ```
7. How to migrate database
    ```
    php artisan migrate --force
    php artisan migrate:install --database=expense_db
    php artisan db:seed --database=expense_db
    ```
- Drop All Tables & Migrate

    ```
    php artisan migrate:fresh 
    php artisan migrate:fresh --seed
    ```
Note: Important when we migrate db
1. In .env
 - DB_HOST=127.0.0.1 or mysql

==================
php artisan config:cache
php artisan config:clear
php artisan clear-compiled
php artisan optimize

=============================
Steps create 1 reponsitory
1- Create 1 inteface file 
2- Create 1 Eloquent file
3- Register service in RepositoryServiceProvider.php file

============================


200: Ok.
201: Đối tượng được tạo, được dùng trong hàm store.
204: Không có nội dung trả về. Hoàn thành hoạt động nhưng sẽ không trả về nội dung gì.
206: Trả lại một phần nội dung, dùng khi sử dụng phân trang.
400: Lỗi. Đây là lỗi cơ bản khi không vượt qua được xác nhận yêu cầu từ server.
401: Unauthorized.
403: Forbidden.
404: Not found.
405 Method Not Allowed
500: Internal server error.
503: Service unavailable.