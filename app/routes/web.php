<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::get('/', function () {
        return response('Api v1', 200)
            ->header('Content-Type', 'text/plain');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', ['uses' => 'CategoriesController@index']);
        Route::get('/{id}', ['uses' => 'CategoriesController@getCategoryById'])->where('id', '[0-9]+');
        Route::post('/', ['uses' => 'CategoriesController@create']);
        Route::put('/{id}', ['uses' => 'CategoriesController@update']);
        Route::delete('/{id}', ['uses' => 'CategoriesController@delete']);
    });

    Route::group(['prefix' => 'expenses'], function () {
        Route::get('/', ['uses' => 'ExpensesController@index']);
        Route::get('/{id}', ['uses' => 'ExpensesController@getExpenseById'])->where('id', '[0-9]+');
        Route::post('/', ['uses' => 'ExpensesController@create']);
        Route::put('/{id}', ['uses' => 'ExpensesController@update']);
        Route::delete('/{id}', ['uses' => 'ExpensesController@delete']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', ['uses' => 'UsersController@index']);
        Route::get('/{id}', ['uses' => 'UsersController@getUserById'])->where('id', '[0-9]+');
        Route::post('/', ['uses' => 'UsersController@insert']);
        Route::put('/{id}', ['uses' => 'UsersController@update']);
    });
});
